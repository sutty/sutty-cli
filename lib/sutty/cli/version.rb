# frozen_string_literal: true

module Sutty
  module Cli
    VERSION = "0.2.1"
  end
end
