# frozen_string_literal: true

require_relative '../command'
require 'tty-file'
require 'yaml'

module Sutty
  module Cli
    module Commands
      class Layout < Sutty::Cli::Command
        attr_reader :name

        def initialize(name, options)
          @name = name
          @options = options
        end

        def execute(input: $stdin, output: $stdout)
          TTY::File.create_file data_layout, YAML.dump(data_default_fields)
          TTY::File.create_file html_layout, "---\nlayout: default\n---\n\n"
        end

        def self.layouts
          @@layouts ||= Dir.glob(File.join('_data', 'layouts') + '/*.yml').map do |d|
            File.basename d, '.yml'
          end
        end

        private

        def data_default_fields
          @data_default_fields ||= {
            'title' => {
              'type' => 'string',
              'label' => {
                'es' => 'Título',
                'en' => 'Title'
              },
              'help' => {
                'es' => '',
                'en' => ''
              }
            }
          }
        end

        def data_layout
          @data_layout ||= File.join('_data', 'layouts', name + '.yml')
        end

        def html_layout
          @html_layout ||= File.join('_layouts', name + '.html')
        end
      end
    end
  end
end
