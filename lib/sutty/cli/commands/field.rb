# frozen_string_literal: true

require_relative '../command'
require 'tty-file'
require 'tty-logger'

module Sutty
  module Cli
    module Commands
      class Field < Sutty::Cli::Command
        attr_reader :name, :options

        INVERSE_REQUIRED = %w[has_many belongs_to has_and_belongs_to_many].freeze
        VALUE_REQUIRED = %w[-F --filter].freeze

        def initialize(name, options)
          @name = name
          @options = options
        end

        def execute(input: $stdin, output: $stdout)
          unless data_layout_contents.scan(/\n#{name}:\n/).empty?
            logger.info "The #{name} field is already present, please edit #{data_layout}"
            return true
          end

          TTY::File.safe_append_to_file(data_layout) do
            ERB.new(template_contents, trim_mode: '<>', eoutvar: '@output_buffer').result(context)
          end
        end

        def self.fields
          @@fields ||= Dir.glob(source_dir.to_s + '/*.yml.erb').map do |f|
            File.basename f, '.yml.erb'
          end
        end

        def self.inverse_required?
          ARGV.any? { |f| INVERSE_REQUIRED.include? f }
        end

        def self.value_required?
          ARGV.any? { |f| VALUE_REQUIRED.include? f }
        end

        private

        def self.source_dir
          @@source_dir ||= Pathname(__dir__).join('..', 'templates', 'field')
        end

        def source_dir
          self.class.source_dir
        end

        def context
          return @context if @context
          @context = OpenStruct.new(**options.transform_keys(&:to_sym))
          @context[:name] = name

          @context = @context.instance_eval('binding')
        end

        def template
          @template ||= source_dir.join(options[:type] + '.yml.erb')
        end

        def template_contents
          @template_contents ||= File.binread template
        end

        def data_layout
          @data_layout ||= File.join('_data', 'layouts', options[:layout] + '.yml')
        end

        def data_layout_contents
          @data_layout_contents ||= File.binread data_layout
        end

        def logger
          @logger ||= TTY::Logger.new
        end
      end
    end
  end
end
