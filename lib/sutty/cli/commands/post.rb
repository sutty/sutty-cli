# frozen_string_literal: true

require_relative '../command'
require 'tty-logger'
require 'tty-file'
require 'yaml'
require 'securerandom'
require 'jekyll/utils'
require 'faker'

module Sutty
  module Cli
    module Commands
      class Post < Sutty::Cli::Command
        attr_reader :options

        CONTENT_FIELDS = %w[content markdown_content].freeze

        def initialize(options)
          @options = options.to_h
          @options['title'] ||= random_string(10)
        end

        def execute(input: $stdin, output: $stdout)
          if File.exist? path
            logger.info "The file #{path} already exists"
            return true
          end

          TTY::File.create_file path,
                                YAML.dump(data) +
                                "---\n\n" +
                                (content? ? random_markdown : '')
        end

        private

        def data
          return @data if @data

          @data = (data_layout.keys - CONTENT_FIELDS).map do |k|
            [k, generate_data(k)]
          end.to_h

          # Required fields
          @data['title']  = options['title']
          @data['layout'] = options['layout']
          @data['uuid']   = SecureRandom.uuid
          @data['liquid'] = false

          @data
        end

        def data_layout
          @data_layout ||= YAML.safe_load(File.read(File.join('_data', 'layouts', options['layout'] + '.yml')))
        end

        def slug
          @slug ||= Jekyll::Utils.slugify(options['title'])
        end

        def dir
          @dir ||= '_' + options['locale']
        end

        def path
          @path ||= File.join(dir, options['date'] + '-' + slug + '.markdown')
        end

        def logger
          @logger ||= TTY::Logger.new
        end

        def generate_data(key)
          # TODO: Generate private data
          return if data_layout[key]['private']
          # Sometimes data is optional
          return unless data_layout[key]['required'] || random_boolean

          case data_layout[key]['type']
          when 'string' then random_text(1)
          when 'text' then random_text(3)
          when 'markdown' then random_markdown(3)
          when 'number' then random_number(255)
          when 'order' then random_number(255)
          when 'tel' then Faker::PhoneNumber.phone_number
          when 'date' then Faker::Date.in_date_period
          when 'array' then Array.new(random_number(10)) { random_string(random_number(3)) }
          when 'predefined_array' then data_layout[key]['values'].keys.sample
          when 'boolean' then random_boolean
          when 'color' then random_color
          when 'email' then Faker::Internet.email
          when 'url' then Faker::Internet.url
          when 'file' then random_file
          when 'image' then random_file
          when 'belongs_to' then random_post(key)
          when 'has_many' then random_posts(key)
          when 'has_and_belongs_to_many' then random_posts(key)
          when 'locales' then random_posts(key)
          when 'related_posts' then random_posts
          when 'geo'
            {
              'lat' => Faker::Address.latitude,
              'lng' => Faker::Address.longitude
            }
          end
        end

        def random_number(digits)
          rand(1..digits)
        end

        def random_text(longitude = nil)
          longitude ||= long? ? random_number(20) : random_number(5)

          Faker::Lorem.paragraphs(number: longitude).join("\n\n")
        end

        def random_string(longitude = random_number(10))
          Faker::Lorem.sentence(word_count: longitude)
        end

        def random_markdown(longitude = nil)
          longitude ||= long? ? random_number(20) : random_number(5)

          Faker::Markdown.sandwich(sentences: longitude,
                                   repeat: long? ? random_number(5) : 1)
        end

        def long?
          options['content'] == 'long'
        end

        def random_color
          Random.bytes(3).unpack1('H*')
        end

        def random_boolean
          @random_boolean ||= [true,false]

          @random_boolean.sample
        end

        def random_file
          {
            'path' => 'public/placeholder.png',
            'description' => random_string(1)
          }
        end

        # TODO: Implement relationships between posts
        def random_post(key = nil)
          SecureRandom.uuid
        end

        def random_posts(key = nil)
          Array.new(random_number(10)) { random_post(key) }
        end

        def types
          @types ||= data_layout.values.map { |v| v['type'] }.uniq
        end

        def content?
          @content ||= CONTENT_FIELDS.map { |f| types.include? f }.any?
        end
      end
    end
  end
end
