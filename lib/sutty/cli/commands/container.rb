# frozen_string_literal: true

require_relative '../command'
require 'tty-command'

module Sutty
  module Cli
    module Commands
      class Container < Sutty::Cli::Command
        attr_reader :name

        def initialize(name, options)
          @name = name
          @options = options
        end

        def execute(input: $stdin, output: $stdout)
          cmd.run('git clone https://0xacab.org/sutty/containers/skel.git', name)

          Dir.chdir name do
            cmd.run('git remote rename origin upstream')
            cmd.run('git remote add origin', origin)
            cmd.run('git push -u origin master')
          end
        end

        private

        def origin
          @origin ||= 'git@0xacab.org:sutty/containers/' + name + '.git'
        end

        def cmd
          @cmd ||= TTY::Command.new
        end
      end
    end
  end
end
