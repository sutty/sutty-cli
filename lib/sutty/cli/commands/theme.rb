# frozen_string_literal: true

require_relative '../command'
require 'tty-command'

module Sutty
  module Cli
    module Commands
      class Theme < Sutty::Cli::Command
        attr_reader :name

        def initialize(name, options)
          @name = name
          @options = options
        end

        def execute(input: $stdin, output: $stdout)
          cmd.run('git clone https://0xacab.org/sutty/jekyll/sutty-base-jekyll-theme.git', theme_name)

          Dir.chdir theme_name do
            cmd.run('git remote rename origin upstream')
            cmd.run('git remote add origin', origin)
            cmd.run('git push -u origin master')
            cmd.run('bundle install')
            cmd.run('yarn install')
            cmd.run('git mv sutty-base-jekyll-theme.gemspec', gemspec)
          end
        end

        private

        def theme_name
          @theme_name ||= name + '-jekyll-theme'
        end

        def origin
          @origin ||= 'git@0xacab.org:sutty/jekyll/' + theme_name + '.git'
        end

        def gemspec
          @gemspec ||= theme_name + '.gemspec'
        end

        def cmd
          @cmd ||= TTY::Command.new
        end
      end
    end
  end
end
