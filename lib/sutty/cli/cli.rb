# frozen_string_literal: true

require 'thor'
require 'date'
require_relative 'commands/layout'
require_relative 'commands/field'

module Sutty
  module Cli
    # Handle the application command line parsing
    # and the dispatch to various command objects
    #
    # @api public
    class CLI < Thor
      # Error raised by this runner
      Error = Class.new(StandardError)

      desc 'version', 'sutty-cli version'
      def version
        require_relative 'version'
        puts "v#{Sutty::Cli::VERSION}"
      end
      map %w(--version -v) => :version

      desc 'post', 'Adds a post'
      method_option :help, aliases: '-h', type: :boolean,
                           desc: 'Display usage information'

      method_option :layout,
                    aliases: '-l',
                    type: :string,
                    desc: 'Layout',
                    required: true,
                    enum: Sutty::Cli::Commands::Layout.layouts

      method_option :title,
                    aliases: '-t',
                    type: :string,
                    desc: 'Title'

      method_option :date,
                    aliases: '-d',
                    type: :string,
                    desc: 'Date',
                    default: Date.today.to_s

      # TODO: Bring locales from Jekyll configuration
      method_option :locale,
                    aliases: '-L',
                    type: :string,
                    desc: 'Locale collection',
                    default: 'posts'

      method_option :content,
                    aliases: '-c',
                    type: :string,
                    desc: 'Text length',
                    default: 'short',
                    enum: %w[short long]

      def post
        if options[:help]
          invoke :help, ['post']
        else
          require_relative 'commands/post'
          Sutty::Cli::Commands::Post.new(options).execute
        end
      end

      desc 'container NAME', 'Adds a container'
      method_option :help, aliases: '-h', type: :boolean,
                           desc: 'Display usage information'
      def container(name)
        if options[:help]
          invoke :help, ['container']
        else
          require_relative 'commands/container'
          Sutty::Cli::Commands::Container.new(name, options).execute
        end
      end

      desc 'field NAME', 'Adds a field with a type to a layout'
      long_desc <<~EOD
        A field is a data type with a name, from which Sutty can build a
        form on the panel and use it to validate posts.

        After you add a field, edit the layout file and add labels and
        help description in different languages.
      EOD

      method_option :help,
                    aliases: '-h',
                    type: :boolean,
                    desc: 'Display usage information'

      method_option :layout,
                    aliases: '-l',
                    type: :string,
                    desc: 'Layout to add',
                    required: true,
                    enum: Sutty::Cli::Commands::Layout.layouts

      method_option :type,
                    aliases: '-t',
                    type: :string,
                    desc: 'Field type',
                    required: true,
                    enum: Sutty::Cli::Commands::Field.fields

      method_option :required,
                    aliases: '-r',
                    type: :boolean,
                    desc: 'Field is required'

      method_option :private,
                    aliases: '-p',
                    type: :boolean,
                    desc: 'Field is private (encrypted)'

      method_option :inverse,
                    aliases: '-i',
                    type: :string,
                    desc: 'Inverse relation',
                    required: Sutty::Cli::Commands::Field.inverse_required?

      method_option :filter,
                    aliases: '-F',
                    type: :string,
                    desc: 'Filter relations by this field'

      method_option :value,
                    aliases: '-v',
                    type: :string,
                    desc: 'Filter relations by this field value',
                    required: Sutty::Cli::Commands::Field.value_required?

      def field(name)
        if options[:help]
          invoke :help, ['field']
        else
          Sutty::Cli::Commands::Field.new(name, options).execute
        end
      end

      desc 'layout NAME', 'Start a new layout'
      method_option :help, aliases: '-h', type: :boolean,
                           desc: 'Display usage information'
      def layout(name)
        if options[:help]
          invoke :help, ['layout']
        else
          Sutty::Cli::Commands::Layout.new(name, options).execute
        end
      end

      desc 'theme NAME', 'Start a new theme'
      method_option :help, aliases: '-h', type: :boolean,
                           desc: 'Display usage information'
      def theme(name)
        if options[:help]
          invoke :help, ['theme']
        else
          require_relative 'commands/theme'
          Sutty::Cli::Commands::Theme.new(name, options).execute
        end
      end
    end
  end
end
