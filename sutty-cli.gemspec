require_relative 'lib/sutty/cli/version'

Gem::Specification.new do |spec|
  spec.name          = "sutty-cli"
  spec.license       = "MIT"
  spec.version       = Sutty::Cli::VERSION
  spec.authors       = ["f"]
  spec.email         = ["f@sutty.nl"]

  spec.summary       = %q{Sutty CLI Tools}
  spec.description   = %q{Tools to develop Sutty's themes and plugins}
  spec.homepage      = "https://0xacab.org/sutty/#{spec.name}"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.6.0")

  spec.metadata = {
    'bug_tracker_uri' => "#{spec.homepage}/issues",
    'homepage_uri' => spec.homepage,
    'source_code_uri' => spec.homepage,
    'changelog_uri' => "#{spec.homepage}/-/blob/master/CHANGELOG.md",
    'documentation_uri' => "https://rubydoc.info/gems/#{spec.name}"
  }

  spec.files         = Dir['lib/**/*', 'exe/*']
  spec.require_paths = %w[lib]
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }

  spec.extra_rdoc_files = Dir['README.md', 'CHANGELOG.md', 'LICENSE.txt']
  spec.rdoc_options += [
    '--title', "#{spec.name} - #{spec.summary}",
    '--main', 'README.md',
    '--line-numbers',
    '--inline-source',
    '--quiet'
  ]

  spec.add_dependency 'tty-command', '~> 0'
  spec.add_dependency 'tty-file', '~> 0'
  spec.add_dependency 'tty-logger', '~> 0'
  spec.add_dependency 'jekyll', '~> 4'
  spec.add_dependency 'thor', '~> 1'
  spec.add_dependency 'faker', '~> 2.14'
  spec.add_development_dependency 'tty', '~> 0'
  spec.add_development_dependency 'pry', '~> 0'
end
