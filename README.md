# Sutty CLI

Tools to ease Sutty's themes and plugins development.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'sutty-cli'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install sutty-cli

## Usage

```bash
sutty-cli help
```

## Development

After checking out the repo, run `bin/setup` to install
dependencies. Then, run `rake spec` to run the tests. You can also run
`bin/console` for an interactive prompt that will allow you to
experiment.

To install this gem onto your local machine, run `bundle exec rake
install`. To release a new version, update the version number in
`version.rb`, and then run `bundle exec rake release`, which will create
a git tag for the version, push git commits and tags, and push the
`.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on 0xacab at
https://0xacab.org/sutty/sutty-cli. This project is intended to be
a safe, welcoming space for collaboration, and contributors are expected
to adhere to the [Sutty code of
conduct](https://sutty.nl/en/code-of-conduct/).

If you like our work, [please consider
donating](https://donaciones.sutty.nl/en/)!

## Code of Conduct

Everyone interacting in the sutty-cli project’s codebases,
issue trackers, chat rooms and mailing lists is expected to follow the
[code of conduct](https://sutty.nl/en/code-of-conduct/).

## Copyright

The gem is available as free software under the terms of the MIT Antifa
License.
